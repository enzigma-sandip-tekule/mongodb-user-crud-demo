import { MongoClient, ObjectID } from 'mongodb';
require('dotenv').config()

interface IUserDao {
	getAll: () => Promise<any[]>;
	add: (user: any) => Promise<void>;
	update: (user: any) => Promise<void>;
	replace: (user: any) => Promise<void>;
	delete: (_id: string) => Promise<void>;
}

export class Database implements IUserDao {

	private db: any;

	private async getDBConnection() {
		const context = this;
		if (context.db) {
			return;
		}
		const url = String(process.env.CONNECTION_STRING);
		const connection = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
		context.db = connection.db();
	}

	public async getAll(): Promise<any[]> {
		try {
			await this.getDBConnection();
			const users = await this.db.collection('user').find({}).toArray();
			return users;
		} catch (err) {
			throw err;
		}
	}

	public async add(user: any): Promise<void> {
		try {
			await this.getDBConnection();
			const result = await this.db.collection('user').insertOne(user);
			return result;
		} catch (err) {
			throw err;
		}
	}

	public async update(user: any): Promise<void> {
		try {
			await this.getDBConnection();
			const userId = new ObjectID(user._id);
			delete user._id;
			const result = await this.db.collection('user').updateOne(
				{ _id: userId }, { $set: user }
			);
			return result;
		} catch (err) {
			throw err;
		}
	}

	public async replace(user: any): Promise<void> {
		try {
			await this.getDBConnection();
			const userId = new ObjectID(user._id);
			delete user._id;
			const result = await this.db.collection('user').replaceOne(
				{ _id: userId }, user
			);
			return result;
		} catch (err) {
			throw err;
		}
	}

	public async delete(id: string): Promise<void> {
		try {
			await this.getDBConnection();
			const result = await this.db.collection('user').deleteOne({ _id: new ObjectID(id) });
			return result;
		} catch (err) {
			throw err;
		}
	}
}
