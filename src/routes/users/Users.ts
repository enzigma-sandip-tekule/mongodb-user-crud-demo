import { Database } from './../../databse';
import { Request, Response, Router } from 'express';
import { BAD_REQUEST, CREATED, OK } from 'http-status-codes';

// Init shared
const router = Router();
export const paramMissingError = 'One or more of the required parameters was missing.';

// Init routes
export const getUsersPath = '/all';
export const addUserPath = '/add';
export const updateUserPath = '/update';
export const replaceUserPath = '/replace';
export const deleteUserPath = '/delete/:id';

const database = new Database();
/******************************************************************************
 *                      Get All Users - "GET /api/users/all"
 ******************************************************************************/

router.get(getUsersPath, async (req: Request, res: Response) => {
	try {
		const users = await database.getAll();
		return res.status(OK).json({ users });
	} catch (err) {
		return res.status(BAD_REQUEST).json({
			error: err.message,
		});
	}
});

/******************************************************************************
 *                       Add One - "POST /api/users/add"
 ******************************************************************************/

router.post(addUserPath, async (req: Request, res: Response) => {
	try {
		const { user } = req.body;
		if (!user) {
			return res.status(BAD_REQUEST).json({
				error: paramMissingError,
			});
		}
		await database.add(user);
		return res.status(CREATED).end();
	} catch (err) {
		return res.status(BAD_REQUEST).json({
			error: err.message,
		});
	}
});

/******************************************************************************
 *                       Update - "PUT /api/users/update"
 ******************************************************************************/

router.put(updateUserPath, async (req: Request, res: Response) => {
	try {
		const { user } = req.body;
		if (!user) {
			return res.status(BAD_REQUEST).json({
				error: paramMissingError,
			});
		}
		await database.update(user);
		return res.status(OK).end();
	} catch (err) {
		return res.status(BAD_REQUEST).json({
			error: err.message,
		});
	}
});

router.put(replaceUserPath, async (req: Request, res: Response) => {
	try {
		const { user } = req.body;
		if (!user) {
			return res.status(BAD_REQUEST).json({
				error: paramMissingError,
			});
		}
		await database.replace(user);
		return res.status(OK).end();
	} catch (err) {
		return res.status(BAD_REQUEST).json({
			error: err.message,
		});
	}
});

/******************************************************************************
 *                    Delete - "DELETE /api/users/delete/:id"
 ******************************************************************************/

router.delete(deleteUserPath, async (req: Request, res: Response) => {
	try {
		await database.delete(req.params.id);
		return res.status(OK).end();
	} catch (err) {
		return res.status(BAD_REQUEST).json({
			error: err.message,
		});
	}
});

/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
