import createError from 'http-errors';
import express from 'express';
import { Request, Response } from 'express';
import cors from 'cors';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';

import indexRouter from './src/routes/index';


const app = express();

// view engine setup
const viewsDir = path.join(__dirname, 'views');
app.set('views', viewsDir);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api', indexRouter);

app.get('*', (req: Request, res: Response) => {
	res.sendFile('index.html', { root: viewsDir });
});

// error handler
app.use(function (err: any, req: any, res: any, next: any) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
