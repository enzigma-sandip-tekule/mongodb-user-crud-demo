

/**
 * Get users
 */
var users = [];
displayUsers();


/**
 * Add User
 */
document.addEventListener('click', function (event) {

	// If the clicked element doesn't have the right selector, bail
	if (!event.target.matches('#add-user-btn')) return;

	// Don't follow the link
	event.preventDefault();

	// Setup data
	let data = {
		user: {
			name: (document.getElementById('name-input').value),
			email: (document.getElementById('email-input').value)
		},
	};

	HttpPost('/api/users/add', data)
		.then(() => {
			displayUsers();
		})

}, false);


/**
 * Display Edit mode
 */
document.addEventListener('click', function (event) {

	// If the clicked element doesn't have the right selector, bail
	if (!event.target.matches('.edit-user-btn')) return;

	// Don't follow the link
	event.preventDefault();

	// Get display/edit divs
	let userEle = event.target.parentNode.parentNode;
	let normalView = userEle.getElementsByClassName('normal-view')[0];
	let editView = userEle.getElementsByClassName('edit-view')[0];

	normalView.style.display = 'none';
	editView.style.display = 'block';

}, false);


/**
 * Display Normal mode (Cancel button)
 */
document.addEventListener('click', function (event) {

	// If the clicked element doesn't have the right selector, bail
	if (!event.target.matches('.cancel-edit-btn')) return;

	// Don't follow the link
	event.preventDefault();

	// Get display/edit divs
	let userEle = event.target.parentNode.parentNode;
	let normalView = userEle.getElementsByClassName('normal-view')[0];
	let editView = userEle.getElementsByClassName('edit-view')[0];

	normalView.style.display = 'block';
	editView.style.display = 'none';

}, false);


/**
 * Edit user
 */
document.addEventListener('click', function (event) {

	// If the clicked element doesn't have the right selector, bail
	if (!event.target.matches('.submit-edit-btn')) return;

	// Don't follow the link
	event.preventDefault();

	// Get id

	// Get id
	let id = event.target.getAttribute('data-user-id');
	const index = users.findIndex(x => x._id === id);
	let user = users[index];
	let userEle = event.target.parentNode.parentNode;

	Object.keys(user).forEach(key => {
		if (key !== '_id')
			user[key] = userEle.getElementsByClassName(`${key}-edit-input`)[0].value;
	});

	let data = {
		user
	};

	HttpPut('/api/users/update', data)
		.then(() => {
			displayUsers();
		})

}, false);


/**
 * Delete user
 */
document.addEventListener('click', function (event) {

	// If the clicked element doesn't have the right selector, bail
	if (!event.target.matches('.delete-user-btn')) return;

	// Don't follow the link
	event.preventDefault();

	// Get id
	let id = event.target.getAttribute('data-user-id');

	HttpDelete('/api/users/delete/' + id)
		.then(() => {
			displayUsers();
		})

}, false);

/**
 * addProperty- user
 */
document.addEventListener('click', function (event) {

	// If the clicked element doesn't have the right selector, bail
	if (!event.target.matches('.addProperty-user-btn')) return;

	// Don't follow the link
	event.preventDefault();

	// Get id
	let id = event.target.getAttribute('data-user-id');
	const index = users.findIndex(x => x._id === id);
	let user = users[index];
	const propertyName = 'field-' + Math.floor(Math.random() * 100);
	user[propertyName] = '';

	let data = {
		user
	};

	HttpPut('/api/users/update', data)
		.then(() => {
			displayUsers();
		})

}, false);

var removeField = function (id, propertyName) {
	const index = users.findIndex(x => x._id === id);
	let user = users[index];
	delete user[propertyName]

	let data = {
		user
	};

	HttpPut('/api/users/replace', data)
		.then(() => {
			displayUsers();
		})
}


function getUserDisplayEle(user) {

	let normalViewFields = '';
	let editViewFields = '';
	Object.keys(user).forEach(key => {
		if (key !== '_id') {
			normalViewFields += `<div><button class="remove" title="remove field" data-user-id="${user._id}"  onclick="removeField('${user._id}','${key}')">-</button> ${key}: ${user[key]}</div>`;

			editViewFields += `<div>${key}: <input class="${key}-edit-input" value="${user[key]}"></div>`;
		}
	});
	let htmlString = `<div class="user-display-ele">
        <div class="normal-view">
            ${normalViewFields}
            <button class="edit-user-btn" data-user-id="${user._id}">
                Edit
            </button>
            <button class="delete-user-btn" data-user-id="${user._id}">
                Delete
			</button>
			<button class="addProperty-user-btn" title="add field"  data-user-id="${user._id}">
			Add Field
		</button>
        </div>

        <div class="edit-view">
			${editViewFields}
            <button class="submit-edit-btn" data-user-id="${user._id}">
                Submit
            </button>
            <button class="cancel-edit-btn" data-user-id="${user._id}">
                Cancel
			</button>

        </div>
	</div>`;

	return htmlString;
}

function displayUsers() {
	HttpGet('/api/users/all')
		.then(response => response.json())
		.then((response) => {
			let allUsers = response.users;
			users = allUsers;
			// Empty the anchor
			let allUsersAnchor = document.getElementById('all-users-anchor');
			allUsersAnchor.innerHTML = '';

			// Append users to anchor
			allUsers.forEach((user) => {
				allUsersAnchor.innerHTML += getUserDisplayEle(user);
			});
		});
}

function HttpGet(path) {
	let options = getSharedOptions();
	options.method = 'GET';
	return fetch(path, options)
}

function HttpPost(path, data) {
	let options = getSharedOptions();
	options.method = 'POST';
	options.body = JSON.stringify(data);
	return fetch(path, options)
}

function HttpPut(path, data) {
	let options = getSharedOptions();
	options.method = 'PUT';
	options.body = JSON.stringify(data);
	return fetch(path, options)
}

function HttpDelete(path) {
	let options = getSharedOptions();
	options.method = 'DELETE';
	return fetch(path, options)
}

function getSharedOptions() {
	return {
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}
}
